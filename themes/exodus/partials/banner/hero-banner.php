<?php

// Set up the height class
$height_class = decide_banner_height_class();

// Text Color and Overlay
$txt_color = get_field( 'text_color' );
$overlay = get_field( 'overlay' );

if ( empty( $overlay ) ) {
	$overlay = "dark-overlay";
}
if ( empty( $txt_color ) ) {
	$txt_color = "light-text";
}

// Figure the Title and Sub Title Out
$title = get_field( 'banner_title' );
if ( empty( $title ) ) {
    $title = decide_banner_title();
}
$subtitle = get_field( 'banner_sub_title' );
if ( empty( $subtitle ) ) {
    $subtitle = decide_banner_subtitle();
}

// Set up thumbnail
$bg_image_url = decide_banner_bg_img();

?>
<section class="page-banner-slider <?php echo $height_class ?>">
    <div class="page-banner__slide <?php echo $txt_color; ?>" style="background-image: url(<?php echo $bg_image_url ?>)">
        <div class="wrap">
            <div class="page-banner__content">
                <h1 class="page-banner__title"><?php echo $title; ?></h1>
                <?php if ( ! empty( $subtitle ) ): ?>
                    <h3 class="page-banner__subtitle"><?php echo $subtitle; ?></h3>
                <?php endif; ?>
            </div>
        </div>


        <?php if ( $overlay != 'none' ): ?>
            <div class="overlay <?php echo $overlay; ?>"></div>
        <?php endif; ?>


    </div>
</section>

<?php
// Decide the banner image
function decide_banner_bg_img() {
    // $thumbnail_id = get_post_thumbnail_id();
    // $thumbnail_url = wp_get_attachment_image_url( $thumbnail_id, 'full' );

    // If we are on an archive or post type lets use that defualt image
    if ( is_home() || is_category() || is_date() || is_singular( 'post' ) ) {
        $default_bg_image_id = get_field( 'default_banner_image_blog', 'options' );
        $bg_image_url = wp_get_attachment_image_url( $default_bg_image_id['ID'], 'full' );
    } elseif ( is_page() ) {
        $default_bg_image_id = get_field( 'banner_image' )['ID'];
        $bg_image_url = wp_get_attachment_image_url( $default_bg_image_id, 'full' );
    } elseif ( is_post_type_archive( 'testimonial' ) || is_singular( 'testimonial' ) ) {
        $default_bg_image_id = get_field( 'default_banner_image_testimonials', 'options' )['ID'];
        $bg_image_url = wp_get_attachment_image_url( $default_bg_image_id, 'full' );
    } elseif ( is_post_type_archive( 'staff' ) || is_singular( 'staff' ) ) {
        $default_bg_image_id = get_field( 'default_banner_image_staff', 'options' )['ID'];
        $bg_image_url = wp_get_attachment_image_url( $default_bg_image_id, 'full' );
    }

    // If default img not empty use it, if its empty use the base default
    if ( empty( $bg_image_url ) ) {
        $default_bg_image_id = get_field( 'default_banner_image', 'options' );
        $bg_image_url = wp_get_attachment_image_url( $default_bg_image_id['ID'], 'full' );
    }

    // if ( ! empty( $thumbnail_url )) {
    //  $bg_image_url = $thumbnail_url;
    // }

    return $bg_image_url;
}

// Decide the banner height
function decide_banner_height_class() {
    $custom_height = get_field( 'banner_custom_height' );
    $banner_height = get_field( 'banner_height' );

    if ( is_home() || is_category() || is_date() || is_singular( 'post' ) ) {
        $default_banner_height = get_field( 'default_banner_height_blog', 'option' );
    } elseif ( is_post_type_archive( 'testimonial' ) || is_singular( 'testimonial' ) ) {
        $default_banner_height = get_field( 'default_banner_height_testimonials', 'option' );
    } elseif ( is_post_type_archive( 'staff' ) || is_singular( 'staff' ) ) {
        $default_banner_height = get_field( 'default_banner_height_staff', 'option' );
    }

    if ( empty( $default_banner_height ) ) {
        $default_banner_height = get_field( 'default_banner_height', 'option' );
    }

    // Set up the banner height class
    $height_class = '';
    if ( !empty( $banner_height ) && $custom_height ) {
        $height_class = $banner_height . '-height-banner';
    } elseif ( !empty( $default_banner_height ) && !$custom_height ) {
        $height_class = $default_banner_height . '-height-banner';
    } else {
        $height_class = 'medium-height-banner';
    }

    return $height_class;
}

// Decide titles for banners
function decide_banner_title() {
    $title = '';
    if ( is_home() || is_category() || is_date() || is_singular( 'post' ) ) {
        $page_for_posts = get_option( 'page_for_posts' );
        $custom_title = get_field( 'archive_title_blog', 'option' );
        $title = get_the_title( $page_for_posts );

        if ( !empty( $custom_title ) ) {
            $title = $custom_title;
        }
    } elseif ( is_post_type_archive( 'testimonial' ) || is_singular( 'testimonial' ) ) {
        $title = get_field( 'archive_title_testimonials', 'option' );
        if ( empty( $title ) ) {
            $title = post_type_archive_title( '', false );
        }
    } elseif ( is_post_type_archive( 'staff' ) || is_singular( 'staff' ) ) {
        $title = get_field( 'archive_title_staff', 'option' );
        if ( empty( $title ) ) {
            $title = post_type_archive_title( '', false );
        }
    } elseif ( is_search() ) {
        $title = 'Search: ' . get_search_query();
    } elseif ( is_404() ) {
        $title = "This page returned a 404";
    } else {
        $title = get_the_title();
    }

    return $title;
}

// Decide Sub titles for banners
function decide_banner_subtitle() {
    $subtitle = '';
    if ( is_home() || is_category() || is_date() || is_singular( 'post' ) ) {
        $subtitle = get_field( 'archive_sub_title_blog', 'option' );
    } elseif ( is_post_type_archive( 'testimonial' ) || is_singular( 'testimonial' ) ) {
        $subtitle = get_field( 'archive_sub_title_testimonials', 'option' );
    } elseif ( is_post_type_archive( 'staff' ) || is_singular( 'staff' ) ) {
        $subtitle = get_field( 'archive_sub_title_staff', 'option' );
    }

    return $subtitle;
}