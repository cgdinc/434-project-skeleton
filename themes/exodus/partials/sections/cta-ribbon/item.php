<?php if ( ! empty($first_text) || ! empty($second_text) && ! empty($btn_details) ) : ?>

	<section class="page-flexible-section ribbon-cta-section-outer <?php echo $padding_classes; ?>" >
		<div class="ribbon-cta-section">
			<div class="wrap">
				<div class="ribbon-content">
					<span class="ribbon-text">
						<?php if ( ! empty( $first_text ) ) : ?>
							<span class="top-text"><?php echo $first_text ?></span>
						<?php endif; ?>

						<?php if ( ! empty( $second_text ) ) : ?>
							<div class="bottom-text"><?php echo $second_text ?></div>
						<?php endif; ?>
					</span>
					<span class="button-wrap">
						<?php echo objectiv_link_button( $btn_details, 'white-button' ); ?>
					</span>
				</div>
			</div>
		</div>
	</section>

<?php endif; ?>