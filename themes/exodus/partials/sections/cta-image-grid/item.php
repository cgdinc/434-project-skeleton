<section class="image-grid-section page-flexible-section <?php echo $padding_classes; ?>">
	<div class="outer-wrap">
			<div class="image-grid-block image-content-block">
				<?php if ( ! empty( $title ) ): ?>
					<h2 class="offset-border"><?php echo $title ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $description ) ): ?>
					<?php echo $description; ?>
				<?php endif; ?>
			</div>
		<?php if ( have_rows( 'images' ) ): ?>
			<?php while( have_rows( 'images' ) ): the_row(); ?>
				<?php $image = get_sub_field( 'image' ); ?>
				<div class="image-grid-block image-block" style="background-image: url('<?php echo $image['url'] ?>')"></div>
			<?php endwhile; ?>
		<?php endif; ?>
		<div class="image-grid-block image-link-block" style="background-image: url('<?php echo $image_with_link['url'] ?>')">
			<div class="image-link-content">
				<?php if ( ! empty( $button_details ) ) : ?>
					<div class="bottom-content">
						<?php echo objectiv_link_button( $button_details ) ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
