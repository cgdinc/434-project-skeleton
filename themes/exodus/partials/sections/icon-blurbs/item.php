<section class="icon-blurb-section page-flexible-section <?php echo $padding_classes; ?> color <?php echo $bg_color; ?>">
	<div class="wrap">
		<div class="icon-blurb-content dark">

			<div class="upper-content">
				<?php obj_section_header($section_title); ?>

				<?php if ( ! empty( $section_sub_title ) ) : ?>
					<h6 class="section-sub-title basemb"><?php echo $section_sub_title ?></h6>
				<?php endif; ?>
			</div>

			<div class="icon-blurb-grid one23grid">
				<?php foreach ($icon_blurbs as $ib ) :
					$blurb_url = $ib['icon_blurb_url'];
					$icon = $ib['icon']['url'];
					$blurb_title = $ib['blurb_title'];
					$blurb = $ib['blurb'];

					?>
					<div class="blurb tac">
						<?php if ( ! empty( $blurb_url ) ) : ?>
							<a href="<?php echo $blurb_url ?>">
						<?php endif; ?>

							<div class="inner-blurb">
								<?php if ( ! empty( $icon ) ) : ?>
									<span>
										<img class="style-svg smallmb" src="<?php echo $icon ?>" alt="<?php echo $blurb_title ?>">
									</span>
								<?php endif; ?>

								<?php if ( ! empty( $blurb_title ) ) : ?>
									<h6 class="section-sub-title"><?php echo $blurb_title ?></h6>
								<?php endif; ?>

								<?php if ( ! empty( $blurb ) ) : ?>
									<p class="section-post-title"><?php echo $blurb ?></p>
								<?php endif; ?>
							</div>

						<?php if ( ! empty( $blurb_url ) ) : ?>
							</a>
						<?php endif; ?>
					</div>

				<?php endforeach; ?>
			</div>


			<?php if ( ! empty( $button_details ) ) : ?>
				<div class="bottom-content">
					<?php echo objectiv_link_button( $button_details ) ?>
				</div>
			<?php endif; ?>

		</div>

	</div>
</section>
