<?php if ( $loop->have_posts() ): ?>
    <section class="page-flexible-section page-section-stories <?php echo $padding_classes; ?> <?php echo $class; ?>">
    	<div class="wrap">
		<?php obj_section_header($title); ?>
    		<div class="stories">
            	<?php while ( $loop->have_posts() ): $loop->the_post(); ?>
            		<div class="story">
                        <?php 
                        $thumbnail = get_the_post_thumbnail_url( $post->ID, 'medium' );
                        if ( empty( $thumbnail ) ) {
                            $default_img = get_field( 'default_banner_image_blog', 'option' );
                            $thumbnail = $default_img['url'];
                        } ?>
                        <?php if ( ! empty( $thumbnail ) ) : ?>
                            <a href="<?php echo $permalink ?>">
                                <div class="story-img" style="background-image: url(' <?php echo $thumbnail ?> ')"></div>
                            </a>
                        <?php endif; ?>

                        <div class="story-blurb">
                            <h5 class="story-title"><?php the_title(); ?></h5>
                            <div class="story-blurb-content">
                                <?php echo objectiv_get_short_description( get_the_ID(), 22 ); ?>
                            </div>
                            <span class="button">
                                <a href="<?php the_permalink(); ?>">Read More</a>
                            </span>
                        </div>
            		</div>
            	<?php endwhile; ?>
        	<?php wp_reset_postdata(); ?>
        </div>
    </div>
</section>
<?php endif; ?>
