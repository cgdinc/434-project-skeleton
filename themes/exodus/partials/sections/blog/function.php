<?php

return (object) array(
	'acf_name'  => 'blog_section',
	'options'   => (object) array(
	'func'      => function ($padding_classes = '') {
		$p_loc = FlexibleContentSectionUtility::getSectionsDirectory();
		$sb_loc = "$p_loc/blog";
		$item = "$sb_loc/item.php";

			$title = get_sub_field( 'title' );
			$blog_category = get_sub_field( 'blog_category' );
			$class = 'three-posts';

			if ( !empty($blog_category)) {
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => '-1',
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field' => 'term_id',
							'terms' => $blog_category,
						),
					)
				);
				$arch_link = get_term_link( $blog_category );
			} else {
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => '-1'
				);
				$arch_link = get_post_type_archive_link( 'post' );
			}

			$loop = new WP_Query($args);
			
			require($item);

		},
		'has_padding'   => true
	)
);