<?php
/**
 * Functions
 *
 */

// Load in the header file
include_once( get_stylesheet_directory() . '/inc/_layout/header.php' );

// Load in the footer file
include_once( get_stylesheet_directory() . '/inc/_layout/footer.php' );

// Helper functions
include_once( get_stylesheet_directory() . '/inc/_helpers/icons.php' );
include_once( get_stylesheet_directory() . '/inc/_helpers/tribe-events.php' );
include_once( get_stylesheet_directory() . '/inc/_helpers/link-buttons.php' );
include_once( get_stylesheet_directory() . '/inc/_helpers/section-header.php');
include_once( get_stylesheet_directory() . '/inc/_helpers/accordion-row.php' );

// Load in scripts (enqueue all the things)
include_once( get_stylesheet_directory() . '/inc/scripts.php' );

// Load in the custom post types
include_once( get_stylesheet_directory() . '/inc/_post-types/00-load-cpts.php' );

// Load in the custom widgets
include_once( get_stylesheet_directory() . '/inc/_widgets/00-load-widgets.php' );

// Run the banner logic
include_once( get_stylesheet_directory() . '/inc/_layout/banner.php' );

// Run the Woo functions
include_once( get_stylesheet_directory() . '/inc/_woo/woo-functions.php' );

/**
 * Theme Setup
 *
 * This setup function attaches all of the site-wide functions
 * to the correct hooks and filters. All the functions themselves
 * are defined below this setup function.
 *
 */

 // Crop Images
if( false === get_option("medium_crop") )
   add_option("medium_crop", "1");
else
   update_option("medium_crop", "1");

add_action('genesis_setup','child_theme_setup', 15);
function child_theme_setup() {

	define( 'CHILD_THEME_VERSION', filemtime( get_stylesheet_directory() . '/style.css' ) );

	// ** Backend **

	// Structural Wraps
	add_theme_support( 'genesis-structural-wraps', array( 'header', 'nav', 'subnav', 'inner', 'footer-widgets', 'footer' ) );

	// Menus
	add_theme_support( 'genesis-menus', array( 'primary' => 'Primary Navigation Menu', 'mobile-menu' => 'Mobile Navigation Menu' ) );

	//* Reposition the primary navigation menu
	remove_action( 'genesis_after_header', 'genesis_do_nav' );
	// add_action( 'genesis_header_right', 'genesis_do_nav' );

	//* Add HTML5 markup structure
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	// Sidebars
	unregister_sidebar( 'sidebar-alt' );

	// genesis_register_sidebar( array( 'name' => 'Blog Sidebar', 'id' => 'blog-sidebar' ) );

	add_theme_support( 'genesis-footer-widgets', 4 );

	// Remove Unused Page Layouts
	genesis_unregister_layout( 'content-sidebar-sidebar' );
	genesis_unregister_layout( 'sidebar-sidebar-content' );
	genesis_unregister_layout( 'sidebar-content-sidebar' );

	// Remove Unused User Settings
	add_filter( 'user_contactmethods', 'objectiv_contactmethods' );
	add_action( 'admin_init', 'objectiv_remove_user_settings' );

	// Editor Styles
	//add_editor_style( 'editor-style.css' );

	// Reposition Genesis Metaboxes
	remove_action( 'admin_menu', 'genesis_add_inpost_seo_box' );
	// add_action( 'admin_menu', 'objectiv_add_inpost_seo_box' );
	remove_action( 'admin_menu', 'genesis_add_inpost_layout_box' );
	// add_action( 'admin_menu', 'objectiv_add_inpost_layout_box' );

	// Remove Genesis Widgets
	add_action( 'widgets_init', 'objectiv_remove_genesis_widgets', 20 );

	// Remove Genesis Theme Settings Metaboxes
	add_action( 'genesis_theme_settings_metaboxes', 'objectiv_remove_genesis_metaboxes' );

	// Don't update theme
	add_filter( 'http_request_args', 'objectiv_dont_update_theme', 5, 2 );

	// ** Frontend **

	// Remove Edit link
	add_filter( 'genesis_edit_post_link', '__return_false' );

	// Responsive Meta Tag
	add_action( 'genesis_meta', 'objectiv_viewport_meta_tag' );

	// Footer
	remove_action( 'genesis_footer', 'genesis_do_footer' );
	add_action( 'genesis_footer', 'objectiv_footer' );

	// Register Widgets
	add_action( 'widgets_init', function(){
		register_widget( 'objectiv_Contact_Widget' );
	});

	// Remove Blog & Archive Template From Genesis
	add_filter( 'theme_page_templates', 'bourncreative_remove_page_templates' );
	function bourncreative_remove_page_templates( $templates ) {
		unset( $templates['page_blog.php'] );
		unset( $templates['page_archive.php'] );
		return $templates;
	}

}

// ** Backend Functions ** //

/**
 * Customize Contact Methods
 * @since 1.0.0
 *
 * @author Bill Erickson
 * @link http://sillybean.net/2010/01/creating-a-user-directory-part-1-changing-user-contact-fields/
 *
 * @param array $contactmethods
 * @return array
 */
function objectiv_contactmethods( $contactmethods ) {
	unset( $contactmethods['aim'] );
	unset( $contactmethods['yim'] );
	unset( $contactmethods['jabber'] );

	return $contactmethods;
}

/**
 * Remove Use Theme Settings
 *
 */
function objectiv_remove_user_settings() {
	remove_action( 'show_user_profile', 'genesis_user_options_fields' );
	remove_action( 'edit_user_profile', 'genesis_user_options_fields' );
	remove_action( 'show_user_profile', 'genesis_user_archive_fields' );
	remove_action( 'edit_user_profile', 'genesis_user_archive_fields' );
	remove_action( 'show_user_profile', 'genesis_user_seo_fields' );
	remove_action( 'edit_user_profile', 'genesis_user_seo_fields' );
	remove_action( 'show_user_profile', 'genesis_user_layout_fields' );
	remove_action( 'edit_user_profile', 'genesis_user_layout_fields' );
}

/**
 * Register a new meta box to the post / page edit screen, so that the user can
 * set SEO options on a per-post or per-page basis.
 *
 * @category Genesis
 * @package Admin
 * @subpackage Inpost-Metaboxes
 *
 * @since 0.1.3
 *
 * @see genesis_inpost_seo_box() Generates the content in the meta box
 */
function objectiv_add_inpost_seo_box() {

	if ( genesis_detect_seo_plugins() )
		return;

	foreach ( (array) get_post_types( array( 'public' => true ) ) as $type ) {
		if ( post_type_supports( $type, 'genesis-seo' ) )
			add_meta_box( 'genesis_inpost_seo_box', __( 'Theme SEO Settings', 'genesis' ), 'genesis_inpost_seo_box', $type, 'normal', 'default' );
	}

}

/**
 * Register a new meta box to the post / page edit screen, so that the user can
 * set layout options on a per-post or per-page basis.
 *
 * @category Genesis
 * @package Admin
 * @subpackage Inpost-Metaboxes
 *
 * @since 0.2.2
 *
 * @see genesis_inpost_layout_box() Generates the content in the boxes
 *
 * @return null Returns null if Genesis layouts are not supported
 */
function objectiv_add_inpost_layout_box() {

	if ( ! current_theme_supports( 'genesis-inpost-layouts' ) )
		return;

	foreach ( (array) get_post_types( array( 'public' => true ) ) as $type ) {
		if ( post_type_supports( $type, 'genesis-layouts' ) )
			add_meta_box( 'genesis_inpost_layout_box', __( 'Layout Settings', 'genesis' ), 'genesis_inpost_layout_box', $type, 'normal', 'default' );
	}

}

/**
 * Remove Genesis widgets
 *
 * @since 1.0.0
 */
function objectiv_remove_genesis_widgets() {
    unregister_widget( 'Genesis_eNews_Updates'          );
    unregister_widget( 'Genesis_Featured_Page'          );
    unregister_widget( 'Genesis_Featured_Post'          );
    unregister_widget( 'Genesis_Latest_Tweets_Widget'   );
    unregister_widget( 'Genesis_User_Profile_Widget'    );
}

/**
 * Remove Genesis Theme Settings Metaboxes
 *
 * @since 1.0.0
 * @param string $_genesis_theme_settings_pagehook
 */
function objectiv_remove_genesis_metaboxes( $_genesis_theme_settings_pagehook ) {
	//remove_meta_box( 'genesis-theme-settings-feeds',      $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-header',     $_genesis_theme_settings_pagehook, 'main' );
	remove_meta_box( 'genesis-theme-settings-nav',        $_genesis_theme_settings_pagehook, 'main' );
	// remove_meta_box( 'genesis-theme-settings-layout',    $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-breadcrumb', $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-comments',   $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-posts',      $_genesis_theme_settings_pagehook, 'main' );
	remove_meta_box( 'genesis-theme-settings-blogpage',   $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-scripts',    $_genesis_theme_settings_pagehook, 'main' );
}

/**
 * Don't Update Theme
 * @since 1.0.0
 *
 * If there is a theme in the repo with the same name,
 * this prevents WP from prompting an update.
 *
 * @author Mark Jaquith
 * @link http://markjaquith.wordpress.com/2009/12/14/excluding-your-plugin-or-theme-from-update-checks/
 *
 * @param array $r, request arguments
 * @param string $url, request url
 * @return array request arguments
 */

function objectiv_dont_update_theme( $r, $url ) {
	if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
		return $r; // Not a theme update request. Bail immediately.
	$themes = unserialize( $r['body']['themes'] );
	unset( $themes[ get_option( 'template' ) ] );
	unset( $themes[ get_option( 'stylesheet' ) ] );
	$r['body']['themes'] = serialize( $themes );
	return $r;
}

// ** Frontend Functions ** //

//* Display a custom favicon
add_filter( 'genesis_pre_load_favicon', 'objectiv_favicon_filter' );
function objectiv_favicon_filter( $favicon_url ) {
	return '';
}

/**
 * Add Theme options
 *
 * @author Wesley Cole
 * @link http://objectiv.co/
 */

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
        array(
    		'page_title' 	=> 'Theme Settings',
    		'menu_title'	=> 'Theme Settings',
    		'menu_slug' 	=> 'theme-general-settings',
    		'icon_url'		=> 'dashicons-art',
    		'capability'	=> 'edit_posts',
    		'position'		=> 59.5,
    		'redirect'		=> false
    	)
    );
}

// Email obfuscation
function objectiv_hide_email($email) {

  $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
  $key = str_shuffle($character_set); $cipher_text = ''; $id = 'e'.rand(1,999999999);
  for ($i=0;$i<strlen($email);$i+=1) $cipher_text.= $key[strpos($character_set,$email[$i])];
  $script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";';
  $script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
  $script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"';
  $script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")";
  $script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';

  return '<span id="'.$id.'">[javascript protected email address]</span>'.$script;

}

/** * Remove editor menu
 */
function remove_editor_menu() {
	remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);


/**
 * Hide editor for content builder pages.
 *
 */
function objectiv_hide_editor() {

	// Get the Post ID.
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	if( !isset( $post_id ) ) return;

	// Get the name of the Page Template file.
	$template_file = get_post_meta( $post_id, '_wp_page_template', true );

	if( $template_file == 'template-content-builder.php' ) { // edit the template name
		remove_post_type_support('page', 'editor');
	}
}
add_action( 'admin_init', 'objectiv_hide_editor' );

// Dequeue woo cart scripts
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11);
function dequeue_woocommerce_cart_fragments() {
    if ( is_front_page() || is_single() ) wp_dequeue_script('wc-cart-fragments');
}


add_action( 'genesis_before_header', 'objectiv_ie_alert' );
function objectiv_ie_alert(){
    ?>
    <!--[if IE]>
     <div class="alert alert-warning">
       <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/?locale=en">upgrade your browser</a> to improve your experience.', 'sage'); ?>
     </div>
   <![endif]-->
    <?php
}

/*
 * Modify TinyMCE editor to remove H1 & Pre.
 */
add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );
function tiny_mce_remove_unused_formats($init) {
    // Add block format elements you want to show in dropdown
    $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;';
    return $init;
}

add_filter( 'the_generator', '__return_empty_string' );


// Get Short Description
function objectiv_get_short_description( $post_id = null, $length = null ) {

    $excerpt = get_the_excerpt( $post_id );
    $post_type = get_post_type( $post_id );

    $excerpt_length = 55;
    if ( ! empty( $length ) ) {
        $excerpt_length = $length;
    }

    if ( empty( $excerpt ) ) {
        if ( function_exists( 'tribe_events_get_event' ) ) {
            $content = strip_shortcodes( tribe_events_get_event( $post_id )->post_content );
        }

        if ( $post_type === 'service' || $post_type === 'industry' ) {
            $content = strip_shortcodes( get_field('content', $post_id ) );
        }

        if ( empty( $content ) ) {
            $content = get_post_meta( $post_id, 'page_flexible_sections_0_content', true );
        }
        if ( empty( $content ) ) {
            $post = get_post( $post_id );
            $content = strip_shortcodes( $post->post_content );
        }
        $excerpt = $content;
	}
	
	$excerpt = wp_trim_words($excerpt, $excerpt_length);

    return $excerpt;
}

// Pretty Dump of Variables
function ovdump( $data ) {
    print("<pre>".print_r( $data, true )."</pre>");
}